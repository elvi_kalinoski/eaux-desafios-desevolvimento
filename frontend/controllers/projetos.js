function main() {
    openProjetos();
}

function fazGet(url) {
    let request = new XMLHttpRequest()
    request.open("GET", url, false)
    request.send()
    return request.responseText
}

function projetoLinha(valor) {
    linha = document.createElement("tr");
    const completo = listaCompleto(valor.id);
    const atrasado = listaAtrasado(valor.id, valor.dataFim);
    linha.innerHTML = `
            <td>${valor.id}</td>
            <td>${valor.nomeProjeto}</td>
            <td>${valor.dataInicio}</td>
            <td>${valor.dataFim}</td>
            <td>${completo}%</td>
            <td>${atrasado}</td>
            <td>
                <button type="button" onclick="window.location.href='./atividades.html?projeto=${valor.id}'" class="btn btn-primary btn-xs btn-flat">Atividades</button>
                <button type="button" id="btnExcluir" onclick="excluir(${valor.id})" class="btn btn-danger btn-xs btn-flat">Excluir</button>
            </td>
        `;

    return linha;
}

function openModal() {
    $("#modalCadastroProjeto").modal();
}

function openProjetos(valor) {
    let dataProjeto = fazGet("http://localhost:3000/projetos");
    let projetos = JSON.parse(dataProjeto);
    let tabelaProjeto = document.getElementById("tabelaProjeto");
    let tabelaTitulo = document.getElementById("tabelaTitulo");
    projetos.forEach(element => {
        let linha = projetoLinha(element);
        tabelaProjeto.appendChild(linha);
    });
}

function listaCompleto(idProjeto) {
    let dataAtividades = fazGet("http://localhost:3000/atividades/" + idProjeto);
    let atividades = JSON.parse(dataAtividades);
    let contador = 0;
    let contIntem = atividades.length;
    let result = 0;
    let projeto_id = 0;

    atividades.forEach(item => {
        if (item.projeto_id == projeto_id) {
            projeto_id = item.projeto_id;
        } else {
            projeto_id = 0
            projeto_id = item.projeto_id;
        }
        if (item.finalizada == 'Sim') {
            contador++
        }
        result = (contador * 100) / contIntem;
    });
    return result.toFixed(0)
}

function listaAtrasado(id, dataProjeto) {
    let dataAtividades = fazGet("http://localhost:3000/atividades/" + id);
    let atividades = JSON.parse(dataAtividades);
    let result = '';
    var data = new Date();
    var dia = ("0" + data.getDate()).slice(-2);
    var mes = ("0" + (data.getMonth() + 1)).slice(-2);
    var ano4 = data.getFullYear();
    var str_data = dia + '/' + mes + '/' + ano4;

    atividades.forEach(item => {
        if (item.dataFim < dataProjeto) {
            result = "Sim"
        } else {
            result = "Não"
        }
    });
    if (result) {
        return result
    } else {
        return " - "
    }
}

main()