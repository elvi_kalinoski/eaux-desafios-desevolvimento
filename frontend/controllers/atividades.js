var urlP = window.location.search.replace("?", "");
var itens = urlP.split("&");
var valor = itens[0].slice(8);

function fazGet(url) {
    let request = new XMLHttpRequest()
    request.open("GET", url, false)
    request.send()
    return request.responseText
}

function listaAtividades(idProjeto) {
    let dataAtividades = fazGet("http://localhost:3000/atividades/" + idProjeto);
    let atividades = JSON.parse(dataAtividades);
    let tabelaAtividade = document.getElementById("tabelaAtividade");

    atividades.forEach(element => {
        let linha = atividadeLinha(element);
        tabelaAtividade.appendChild(linha);
    });
}

function atividadeLinha(valor) {
    linha = document.createElement("tr");
    linha.innerHTML = `
            <td id="vi">${valor.id}</td>
            <td id="vpi">${valor.projeto_id}</td>
            <td id="vna">${valor.nomeAtividade}</td>
            <td id="vdi">${valor.dataInicio}</td>
            <td id="vdf">${valor.dataFim}</td>
            <td id="vf">${valor.finalizada}</td>
            <td>
                
                <button type="button" onclick="excluirAtividade(${valor.id})" class="btn btn-danger btn-xs btn-flat">Excluir</button>
            </td>
        `;
    return linha;
    // <button type="button" onclick="openModal(${valor.id})" class="btn btn-primary btn-xs btn-flat">Editar</button>
}

function openModal(valor) {
    if (valor) {
        $("#modalAtividades").modal();
        document.getElementById("nomeAtidade").value = valor;
        document.getElementById("dataInicio").value = valor;
        document.getElementById("dataFim").value = valor;
        document.getElementById("idProjeto").value = valor;
        document.getElementById("status").value = valor;
    } else {
        $("#modalAtividades").modal();
        setIdAtividade()
    }
}

function setIdAtividade() {
    document.getElementById("idProjeto").value = valor;
}

listaAtividades(valor)