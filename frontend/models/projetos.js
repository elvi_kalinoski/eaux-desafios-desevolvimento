let url = "http://localhost:3000/projetos/";

function processa(urlV, requestOptions) {
    fetch(urlV, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    document.location.reload(true);
}

function salvar() {
    request(options, function(error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
    document.location.reload(true);
}

function form_submit() {
    document.getElementById("paymentitrform").submit();

    request(options, function(error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
}


function enviar() {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    var urlencoded = new URLSearchParams();
    urlencoded.append("nomeProjeto", document.getElementById("nomeProjetoE").value);
    urlencoded.append("dataInicio", document.getElementById("dataInicioE").value);
    urlencoded.append("dataFim", document.getElementById("dataFimE").value);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };

    processa(url, requestOptions);
}

function excluir(valor) {
    const urlV = url + valor
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
    };
    processa(urlV, requestOptions);
}

function excluirAtividade(valor) {
    const urlV = url + valor
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
    };

    processa(urlV, requestOptions);
}