let url = "http://localhost:3000/atividades/";

function processa(urlV, requestOptions, value) {
    fetch(urlV, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    document.location.reload(true);
}

function salvar() {
    alert('Salvar')
    request(options, function(error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
    document.location.reload(true);
}

function form_submit() {
    document.getElementById("paymentitrform").submit();
    request(options, function(error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
}


function enviar(valor, status) {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    var urlencoded = new URLSearchParams();
    urlencoded.append("nomeAtividade", document.getElementById("nomeAtidade").value);
    urlencoded.append("dataInicio", document.getElementById("dataInicio").value);
    urlencoded.append("dataFim", document.getElementById("dataFim").value);
    urlencoded.append("projeto_id", document.getElementById("idProjeto").value);
    urlencoded.append("finalizada", document.getElementById("status").value);

    var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: urlencoded,
        redirect: 'follow'
    };
    processa(url, requestOptions);
}

function excluir(valor) {
    const urlV = url + valor
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
    };
    processa(urlV, requestOptions);
}

function excluirAtividade(valor) {
    const urlV = url + valor
    var requestOptions = {
        method: 'DELETE',
        redirect: 'follow'
    };
    processa(urlV, requestOptions);
}