# Desafio Web Developer - Grupo EUAX
 Aplicação foi realizada utilizando as seguintes tecnologias.

**REQUISITOS ( NPM E DOCKER )**

### Banco de dados:
* Docker
* MySql

### Backend
* NodeJS

***API feita em nodejs***

### Frontend
* HTML
* Javascript
* Bootstrap

### Iniciando aplicação
Dentro do diretorio executar o `sh start.sh`

[Link de acesso](http://127.0.0.1:8080)

Dentro da pasta documents tem alguns prints das telas

