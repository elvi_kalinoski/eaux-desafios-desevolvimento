#!/bin/bash

# Inicializa o container com frontend e banco
cd docker
docker-compose up -d

# Inicializa o backend
cd ..
cd backend
npm ci
npm start

# x-www-browser http://127.0.0.1:8080