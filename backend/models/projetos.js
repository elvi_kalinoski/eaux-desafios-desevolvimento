const moment = require('moment')
const conexao = require('../db/conexao')

class Projeto {
    adiciona(projeto, res) {
        const dataAtual = moment().format('YYYY-MM-DD')
        const dataInicio = moment(projeto.dataInicio, 'DD/MM/YYYY').format('YYYY-MM-DD')
        const dataFim = moment(projeto.dataFim, 'DD/MM/YYYY').format('YYYY-MM-DD')
        const dataEhValida = moment(dataInicio).isSameOrAfter(dataAtual)
        const nomeProjetoEhValido = projeto.nomeProjeto.length >= 4

        const validacoes = [{
            nome: 'dataInicio',
            valido: dataEhValida,
            mensagem: 'dataInicio deve ser maior ou igual a data atual'
        }, {
            nome: 'nomeProjeto',
            valido: nomeProjetoEhValido,
            mensagem: 'Nome projeto de ter pelo menos 4 caracteres'
        }]

        const erros = validacoes.filter(campo => !campo.valido)
        const existemErros = erros.length

        if (existemErros) {
            res.status(400).json(erros)
        } else {
            const projetoDatado = {...projeto, dataInicio, dataFim }
            const sql = 'INSERT INTO projetos SET ?'

            conexao.query(sql, projetoDatado, (erro, resultados) => {
                if (erro) {
                    res.status(400).json(erro)
                } else {
                    res.status(201).json(projeto)
                }
            })
        }
    }

    lista(res) {
        const idCampo = ''
        const sql = 'SELECT * FROM projetos '

        conexao.query(sql, (erro, resultados) => {
            const result = Object.values(JSON.parse(JSON.stringify(resultados)));

            result.forEach(item => {
                item.dataInicio = moment(item.dataInicio).format('DD/MM/YYYY')
                item.dataFim = moment(item.dataFim).format('DD/MM/YYYY')
            });
            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(201).json(result)
            }
        })
    }

    buscaPorId(id, res) {
        const sql = `SELECT * FROM projetos WHERE id=${id}`
        conexao.query(sql, (erro, resultados) => {
            const projeto = resultados[0]
            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(201).json(projeto)
            }
        })
    }

    altera(id, valores, res) {
        if (valores.data) {
            valores.data = moment(valores.data, 'DD/MM/YYYY').format('YYYY-MM-DD HH:mm:ss')
        }
        const sql = 'UPDATE projetos SET ? WHERE id=?'
        conexao.query(sql, [valores, id], (erro, resultados) => {
            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(201).json({...valores, id })
            }
        })
    }

    deleta(id, res) {
        const sql = 'DELETE FROM projetos WHERE id=?'
        conexao.query(sql, id, (erro, resultados) => {
            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(200).json({ id })
            }
        })
    }
}

module.exports = new Projeto