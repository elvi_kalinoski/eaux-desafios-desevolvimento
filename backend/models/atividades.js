const moment = require('moment')
const conexao = require('../db/conexao')

class Atividade {
    adiciona(atividade, res) {
        const dataAtual = moment().format('YYYY-MM-DD')
        const dataInicio = moment(atividade.dataInicio, 'DD/MM/YYYY').format('YYYY-MM-DD')
        const dataFim = moment(atividade.dataFim, 'DD/MM/YYYY').format('YYYY-MM-DD')
        const dataEhValida = moment(dataInicio).isSameOrAfter(dataAtual)
        const nomeAtividadeEhValido = atividade.nomeAtividade.length >= 4

        const validacoes = [{
            nome: 'dataInicio',
            valido: dataEhValida,
            mensagem: 'dataInicio deve ser maior ou igual a data atual'
        }, {
            nome: 'nomeAtividade',
            valido: nomeAtividadeEhValido,
            mensagem: 'Nome atividade de ter pelo menos 4 caracteres'
        }]

        const erros = validacoes.filter(campo => !campo.valido)
        const existemErros = erros.length

        if (existemErros) {
            res.status(400).json(erros)
        } else {
            const atividadeDatado = {...atividade, dataInicio, dataFim }
            const sql = 'INSERT INTO atividades SET ?'

            conexao.query(sql, atividadeDatado, (erro, resultados) => {
                if (erro) {
                    res.status(400).json(erro)
                } else {
                    res.status(201).json(atividade)
                }
            })
        }
    }

    lista(res) {
        const sql = 'SELECT * FROM atividades'

        conexao.query(sql, (erro, resultados) => {
            const result = Object.values(JSON.parse(JSON.stringify(resultados)));

            result.forEach(item => {
                item.dataInicio = moment(item.dataInicio).format('DD/MM/YYYY')
                item.dataFim = moment(item.dataFim).format('DD/MM/YYYY')
            });

            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(201).json(result)
            }
        })
    }

    buscaPorId(id, res) {
        const sql = `SELECT * FROM atividades WHERE projeto_id=${id}`
        conexao.query(sql, (erro, resultados) => {
            const result = Object.values(JSON.parse(JSON.stringify(resultados)));
            result.forEach(item => {
                item.dataInicio = moment(item.dataInicio).format('DD/MM/YYYY')
                item.dataFim = moment(item.dataFim).format('DD/MM/YYYY')
                if (item.finalizada == 1) {
                    item.finalizada = "Sim"
                } else {
                    item.finalizada = "Não"
                }
            });

            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(201).json(result)
            }
        })

    }

    altera(id, valores, res) {
        if (valores.data) {
            valores.data = moment(valores.data, 'DD/MM/YYYY').format('YYYY-MM-DD HH:mm:ss')
        }
        const sql = 'UPDATE atividades SET ? WHERE id=?'
        conexao.query(sql, [valores, id], (erro, resultados) => {
            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(201).json({...valores, id })
            }

        })
    }

    deleta(id, res) {
        const sql = 'DELETE FROM atividades WHERE id=?'
        conexao.query(sql, id, (erro, resultados) => {
            if (erro) {
                res.status(400).json(erro)
            } else {
                res.status(200).json({ id })
            }
        })
    }

}

module.exports = new Atividade