const projeto = require('../models/projetos');

module.exports = app => {
    app.get('/projetos', (req, res) => {
        projeto.lista(res)
    })

    app.get('/projetos/:id', (req, res) => {
        const id = parseInt(req.params.id)
        projeto.buscaPorId(id, res)
    })

    app.post('/projetos', (req, res) => {
        const dados = req.body
        projeto.adiciona(dados, res)
    })

    app.patch('/projetos/:id', (req, res) => {
        const id = parseInt(req.params.id)
        const valores = req.body
        projeto.altera(id, valores, res)
    })

    app.delete('/projetos/:id', (req, res) => {
        const id = parseInt(req.params.id)
        projeto.deleta(id, res)
    })
}