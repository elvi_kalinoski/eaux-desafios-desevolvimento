const atividade = require('../models/atividades');

module.exports = app => {
    app.get('/atividades', (req, res) => {
        atividade.lista(res)
    })

    app.get('/atividades/:id', (req, res) => {
        const id = parseInt(req.params.id)
        atividade.buscaPorId(id, res)
    })

    app.post('/atividades', (req, res) => {
        const dados = req.body
        atividade.adiciona(dados, res)
    })

    app.patch('/atividades/:id', (req, res) => {
        const id = parseInt(req.params.id)
        const valores = req.body
        atividade.altera(id, valores, res)
    })

    app.delete('/atividades/:id', (req, res) => {
        const id = parseInt(req.params.id)
        atividade.deleta(id, res)
    })
}