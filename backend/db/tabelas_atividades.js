class TabelasAtividades {
    init(conexao) {
        this.conexao = conexao;
        this.criarAtividades();
    }

    criarAtividades() {
        const sql = 'CREATE TABLE IF NOT EXISTS atividades (id int NOT NULL AUTO_INCREMENT, projeto_id INT NOT NULL , nomeAtividade varchar(50) NOT NULL, dataInicio datetime NOT NULL, dataFim datetime NOT NULL, finalizada boolean NOT NULL, PRIMARY KEY(id))'

        this.conexao.query(sql, erro => {
            if (erro) {
                console.log(erro)
            } else {
                console.log('Tabela atividades criada com sucesso')
            }
        })
    }
}

module.exports = new TabelasAtividades