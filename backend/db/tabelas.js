class Tabelas {
    init(conexao) {
        this.conexao = conexao;
        this.criarProjeto();
        this.criarAtividades();
    }

    criarProjeto() {
        const sql = 'CREATE TABLE IF NOT EXISTS projetos (id int NOT NULL AUTO_INCREMENT, nomeProjeto varchar(50) NOT NULL, dataInicio datetime NOT NULL, dataFim datetime NOT NULL, PRIMARY KEY(id))'

        this.conexao.query(sql, erro => {
            if (erro) {
                console.log(erro)
            } else {
                console.log('Tabela projetos criada com sucesso')
            }
        })
    }

    criarAtividades() {
        const sql = 'CREATE TABLE IF NOT EXISTS atividades (id int NOT NULL AUTO_INCREMENT, projeto_id INT NOT NULL , nomeAtividade varchar(50) NOT NULL, dataInicio datetime NOT NULL, dataFim datetime NOT NULL, finalizada boolean NOT NULL, PRIMARY KEY(id))'

        this.conexao.query(sql, erro => {
            if (erro) {
                console.log(erro)
            } else {
                console.log('Tabela atividades criada com sucesso')
            }
        })
    }
}

module.exports = new Tabelas